from .models import Tienda,EstadoTienda
from rest_framework import serializers



#Serializacion para el models de tienda

class EstadoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
            model=EstadoTienda
            fields=('desc',)
    

class TiendaSerializer(serializers.HyperlinkedModelSerializer):
    estado = EstadoSerializer()
    class Meta:
        model=Tienda
        fields=('url','nombre','sucursal','online','url','estado','direccion','ciudad','region' )
        read_only_fields=('EstadoTienda',)

