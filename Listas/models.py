from django.db import models
from django.db.models import Sum
from django.contrib.auth.models import User
from django.dispatch import receiver
from allauth.account.signals import user_signed_up

#Tupla
class EstadoProducto(models.Model):
    desc = models.CharField(max_length=30,blank=False)
    #Agregar estados Pendiente(id=1) y comprado(id=2)
    def __str__(self):
        return self.desc

#Tupla
class EstadoTienda(models.Model):
    desc = models.CharField(max_length=30,blank=False)
    #Agregar estados Pendiente(id=1) y aprobado(id=2)
    def __str__(self):
        return self.desc


class Usuario(models.Model):
    #un usuario deberia tener username, password y un email
    user = models.ForeignKey(User, on_delete=models.CASCADE,default=None)

    #para crear una lista asociada a esta usuario
    def crearLista(self,nombre):
        print(nombre)
        try:
            l = Lista(nombre=nombre,usuario=self)
            l.save()
            return True
        except:
            return False
    def __str__(self):
        return self.user.username



@receiver(user_signed_up)
def create_user_profile(request, user, **kwargs):
    u = Usuario(user=user)
    u.save()



class Lista(models.Model):
    nombre = models.CharField(max_length=120,blank=False, primary_key=True)
    #Varias listas pueden estar asociadas a un usuario
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)

    #para crear un producto asociado a esta lista, requiere una tienda
    def crearProducto(self,producto):
        try:
            p = producto
            p.lista = self
            print(p)
            p.save()
            return True
        except:
            return False

    #retorna un diccionario con el total presupuestado, el total real comprado,
    #la cantidad de productos agregados y la cantidad de productos comprados
    def queryDict(self):
        p = Producto.objects.filter(lista=self).aggregate(tPresupuesto=Sum('cPresupuesto'),tRealComprado=Sum('cReal'))
        p["agregados"] = Producto.objects.filter(lista=self).count()
        p["comprados"] = Producto.objects.filter(lista=self).filter(estado=EstadoProducto.objects.get(pk=2)).count()
        return p

    def __str__(self):
        return self.nombre

class Tienda(models.Model):
    nombre = models.CharField(max_length=120,blank=False)
    sucursal = models.CharField(max_length=120,blank=True)
    #estado por defecto debe ser pendiente
    estado = models.ForeignKey(EstadoTienda,on_delete=models.CASCADE)
    online = models.BooleanField()
    url = models.CharField(max_length=300,blank=True)
    #si online es falso
    direccion = models.CharField(max_length=240,blank=True)
    ciudad = models.CharField(max_length=30,blank=True)
    region = models.CharField(max_length=60,blank=True)

    #Para aprobar la tienda, con el fin de que se pase a la lista
    def aprobar(self):
        self.estado = EstadoTienda.objects.get(pk=2)
        self.save()
    def __str__(self):
        return self.nombre


class Producto(models.Model):
    nombre = models.CharField(max_length=120,blank=False)
    cPresupuesto = models.IntegerField()
    cReal = models.IntegerField(default=0)
    notas = models.CharField(max_length=240)
    #estado por defecto debe ser pendiente
    estado = models.ForeignKey(EstadoProducto,on_delete=models.CASCADE)
    #Varios productos pueden estar asociadas a una lista
    lista = models.ForeignKey(Lista, on_delete=models.CASCADE)
    #varios productos pueden estar asociados a una tienda
    tienda = models.ForeignKey(Tienda, on_delete=models.SET_NULL, null=True)

    #para registrar este producto como comprado, requiere pasarle un costoReal
    def registrarComprado(self, cReal):
        self.cReal = cReal
        self.estado = EstadoProducto.objects.get(pk=2)
        self.save()

    def __str__(self):
        return self.nombre


