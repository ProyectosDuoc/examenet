from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from .models import *
#import de usuarios
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,logout, login as auth_login
from django.contrib.auth.decorators import login_required, user_passes_test
from allauth.account.signals import user_logged_in
#import serializacion
from rest_framework import viewsets
from Listas.serializers import TiendaSerializer,EstadoSerializer
from django.core import serializers

# vistas de navegacion
def index(request):
    usuario = request.session.get('usuario',None)
    #Redirecciona a las listas
    return redirect('listacompras')

def Nuevolocal(request):
    usuario = request.session.get('usuario',None)
    t = Tienda.objects.all()
    return render(request,'AgregarLocal.html',{'titulo':'Solicitar de ingreso -Locales-','usuario':usuario,'tiendas':t})

def ListaC(request):
    usuario = request.session.get('usuario',None)
    if usuario is not None:
        u = User.objects.get(username=usuario)
        if u.is_staff == False:
            u = Usuario.objects.get(user=u)
            l = Lista.objects.filter(usuario=u)
            t = Tienda.objects.filter(estado=EstadoTienda.objects.get(pk=2))
            listadata=[]
            for lista in (l):
                p = Producto.objects.filter(lista=lista)
                listadata.append({'lista':lista,'queryDict':lista.queryDict(),'productos':p})
        else:
            listadata = None
            t = None
    else:
        listadata = None
        t = None
    return render(request,'ListasCompras.html',{'titulo':'Mis Listas','usuario':usuario,'listas':listadata,'tiendas':t})

def crearLista(request):
    nombre = request.POST.get('nombre','')

    usuario = request.session.get('usuario',None)
    u = User.objects.get(username=usuario)
    u = Usuario.objects.get(user=u)
    u.crearLista(nombre=nombre)
    return redirect('listacompras')

def agregarProducto(request, lista):
    nombre = request.POST.get('nombreP','')
    valorP = request.POST.get('valorP','')
    valorR = request.POST.get('valorR','')
    tienda = request.POST.get('tienda','')
    notas = request.POST.get('notas','')

    p = Producto(nombre=nombre,cPresupuesto=valorP,cReal=valorR,notas=notas,estado=EstadoProducto.objects.get(pk=1),tienda=Tienda.objects.get(pk=tienda))
    valorR = int(valorR)
    if valorR > 0:
        p.registrarComprado(cReal=valorR)
    l = Lista.objects.get(nombre=lista)
    l.crearProducto(producto=p)

    return redirect('listacompras')

def eliminarProducto(request, id):
    p = Producto.objects.get(pk=id)
    p.delete()
    return redirect('listacompras')

def eliminarLista(request, lista):
    l = Lista.objects.get(nombre=lista)
    l.delete()
    return redirect('listacompras')

def Productos(request, id):
    usuario = request.session.get('usuario',None)
    p = Producto.objects.get(pk=id)
    t = Tienda.objects.filter(estado=EstadoTienda.objects.get(pk=2))
    return render(request,'ListaProductos.html',{'titulo':'Editar producto '+p.nombre,'usuario':usuario,'producto':p,'tiendas':t})

def editarProducto(request, id):
    p = Producto.objects.get(pk=id)
    p.nombre = request.POST.get('nombre','')
    p.cPresupuesto = request.POST.get('valorP','')
    p.tienda = Tienda.objects.get(pk=request.POST.get('tienda',''))
    p.notas = request.POST.get('notas','')
    cReal = request.POST.get('valorR',0)
    cReal = int(cReal)

    if cReal > 0:
        p.registrarComprado(cReal=cReal)
    p.save()
    return redirect ('listacompras')

def LoginPage(request):
    usuario = request.session.get('usuario',None)
    return render(request,'LoginPage.html',{'titulo':'Login','usuario':usuario})

def RegisterPage(request):
    usuario = request.session.get('usuario',None)
    return render(request,'Registro.html',{'titulo':'Registro','usuario':usuario})

#gestion tiendas
def crearTienda(request):
    nombre = request.POST.get('nombre','')
    sucursal = request.POST.get('sucursal','')
    comuna = request.POST.get('comuna','')
    region = request.POST.get('region','')
    direccion = request.POST.get('direccion','')
    url = request.POST.get('url','')
    
    t = Tienda(nombre=nombre,sucursal=sucursal,ciudad=comuna,region=region,direccion=direccion,url=url)
    t.estado = EstadoTienda.objects.get(pk=1)
    if t.url is not '':
        t.online = True
    else:
        t.online = False
    t.save()
    return redirect('agregarlocal')

@login_required
@user_passes_test(lambda u: u.is_staff)
def aprobarTienda(request, id):
    t = Tienda.objects.get(pk=id)
    t.aprobar()
    return redirect('agregarlocal')

@login_required
@user_passes_test(lambda u: u.is_staff)
def eliminarTienda(request, id):
    t = Tienda.objects.get(pk=id)
    t.delete()
    return redirect('agregarlocal')
#control de Usuarios
def registrarUsuario(request):
    nombre=request.POST.get('nombre','')
    correo=request.POST.get('correo','')
    contrasenia=request.POST.get('contraseña','')

    u = Usuario(user=User.objects.create_user(username=nombre,email=correo,password=contrasenia))
    try:
        u.save()
    except:
        u.user.delete()
        u = None
        return HttpResponse('Se murio')
    return redirect('login')

def inicioSesion(request):
    nombre = request.POST.get('nombre','')
    contrasenia = request.POST.get('passwd','')
    user = authenticate(request,username=nombre, password=contrasenia)

    if user is not None:
        auth_login(request, user)
        request.session['usuario'] = user.username
        return redirect("listacompras")
    else:
        return redirect("login")

@receiver(user_logged_in)
def log_in(request, user, **kwargs):
    request.session['usuario'] = user.username

@login_required
def cerrarSesion(request):
    del request.session['usuario']
    logout(request)
    return redirect('index')

def profileview(request):
    return redirect('listacompras')

#Views Api

class TiendaApi(viewsets.ModelViewSet):
    queryset=Tienda.objects.all()
    serializer_class=TiendaSerializer

class EstadoApi(viewsets.ModelViewSet):
    queryset=EstadoTienda.objects.all()
    serializer_class=EstadoSerializer



def getdata(request):
 results=User.objects.all()
 jsondata = serializers.serialize('json',results)
 return HttpResponse(jsondata)


@login_required
def onesignal_register(request):
  usuario = request.session.get('usuario',None)
  profile = User.objects.get(username=usuario) # The model where you will to save the profile.
  print(profile)
  print(request.POST.get('playerId','no hay'))
  if request.POST.get('playerId'):
      profile.onesignal_playerId = request.POST.get('playerId')
      profile.save()
      return HttpResponse('Done')
  return HttpResponse('Something went wrong')