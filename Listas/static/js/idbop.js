fetch('http://localhost:8000/getdata').then(function(response){
  return response.json();
 }).then(function(jsondata){
  dbPromise.then(function(db){
   var tx = db.transaction('User', 'readwrite');
     var feedsStore = tx.objectStore('User');
     for(var username in jsondata){
      if (jsondata.hasOwnProperty(username)) {
        feedsStore.put(jsondata[username]); 
      }
     }
  });
 });

var dbPromise = idb.open('User-db', 1, function(upgradeDb) {
    upgradeDb.createObjectStore('User',{keyPath:'pk'});
   });

