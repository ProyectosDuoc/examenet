

  $("#btnLogin").click(function(event) {

    //Fetch form to apply custom Bootstrap validation
    var form = $("#formLogin")

    if (form[0].checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
    }
    
    form.addClass('was-validated');
  });





/*Validaciones Locales
  Solo deje activa la validacion respecto a los nombre,
  deje las otras como false, por si s requiere cambiarlos */
$(function(){
  $("#solicitudLocal").validate({
    rules:{
      nombre:{
        required:true
      },
      nombreSucursal:{
        required:true
      },
      region:{
        required:false

      },
      comuna:{
        required:false

      },
      direccion:{
        required:false
      },
      pag:{
        required:false,
        url:true
      }

    }
  });
});

/*Validaciones para el login */
$(function(){
  $("#formLogin").validate({
    rules:{
      nombre:{
        required:true
      },
      passwd:{
        required:true
      }
    }
  });
});

/* Validaciones para el ingreso de productos */

$(function(){
  $("#formProductos").validate({
    rules:{
      nombreP:{
        required:true
      },
      valorP:{
        required:true,
        digits:true
      },
      valorR:{
        required:true,
        digits:true
      },
      tienda:{
        required:true
      },
    }
  });
});

/*Validaciones para el registro */

$(function(){
  $("RegistroForm").validate({
    rules:{
      nombre:{
        required:true
      },
      correo:{
        required:true,
        email:true
      },
      contraseña:{
        required:true
      }
    }
  });
});

/**Validacion para  el ingreso de una nueva lista */

$(function(){
  $("formListas").validate({
    rules:{
      nombreL:{
        required:true
      }
    }
  });
});

/*Da las nostificaciones de manera generica, pra no cambiar una por una para las otras validaciones  */
$(document).ready(function() {
  jQuery.extend(jQuery.validator.messages, {
    required: "campo obligatorio.",
    remote: "Por favor, rellena este campo.",
    email: "Por favor, escribe una dirección de correo válida",
    url: "Por favor, escribe una URL válida.",
    date: "Por favor, escribe una fecha válida.",
    dateISO: "Por favor, escribe una fecha (ISO) válida.",
    number: "Por favor, escribe un número entero válido.",
    digits: "Por favor, escribe sólo dígitos.",
    creditcard: "Por favor, escribe un número de tarjeta válido.",
    equalTo: "Por favor, escribe el mismo valor de nuevo.",
    accept: "Por favor, escribe un valor con una extensión aceptada.",
    maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
    minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
    rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
    range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
    max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
    min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
  });
  });





