from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path
from . import views
from django.dispatch import receiver
from allauth.account.signals import user_signed_up
#import api
from django.conf.urls import  url,include
from rest_framework import routers

from django.views.generic import TemplateView

#Api
router=routers.DefaultRouter()
router.register(r'Tiendas',views.TiendaApi)

urlpatterns = [
    #navegacion
    path('',views.index, name='index'),
    path('locales/', views.Nuevolocal, name='agregarlocal'),
    path('listas/', views.ListaC, name='listacompras'),
    path('productos/<int:id>', views.Productos, name='productos'),
    path('login/', views.LoginPage, name='login'),
    path('registro/', views.RegisterPage, name='registro'),
    #usuarios
    path('registro/registrarUsuario', views.registrarUsuario, name='registrarUsuario'),
    path('login/inicioSesion', views.inicioSesion, name='inicioSesion'),
    path('cerrarSesion/', views.cerrarSesion, name='cerrarSesion'),
    #tiendas
    path('locales/aprobar/<int:id>',views.aprobarTienda, name='aprobarTienda'),
    path('locales/eliminar/<int:id>',views.eliminarTienda, name='eliminarTienda'),
    path('locales/crearTienda', views.crearTienda, name='crearTienda'),
    #listas
    path('listas/crearLista',views.crearLista, name='crearLista'),
    path('listas/agregarProducto/<str:lista>',views.agregarProducto, name='agregarProducto'),
    path('productos/editarProducto/<int:id>',views.editarProducto,name='editarProducto'),
    path('listas/eliminarProducto/<int:id>',views.eliminarProducto,name='eliminarProducto'),
    path('listas/eliminarLista/<str:lista>',views.eliminarLista,name='eliminarlista'),
    #allauth
    path('accounts/', include('allauth.urls')),
    path('accounts/profile/', views.profileview, name="facebook"),

    path('getdata/', views.getdata, name="getdata"),
    #API
    url(r'^',include(router.urls)),
    url(r'^api-auth/',include('rest_framework.urls',namespace='rest_framework')),

    path('manifest.json', TemplateView.as_view(template_name='manifest.json', content_type='application/json')),
    path('OneSignalSDKWorker.js', TemplateView.as_view(template_name='OneSignalSDKWorker.js', content_type='application/x-javascript')),
    path('OneSignalSDKUpdaterWorker.js', TemplateView.as_view(template_name='OneSignalSDKUpdaterWorker.js', content_type='application/x-javascript')),
    path('onesignal_register/', views.onesignal_register, name='onesignal_register'),
    ] 